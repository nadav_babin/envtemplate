var express = require("express");
var path = require('path');
var mongoose = require("mongoose");
var config = require("./config");
var bodyParser = require("body-parser");
var morgan = require("morgan");
var port  = config.port ? config.port : 80;
var fs = require('fs');
var routesFolder = './server/routes/';

var app = express();
app.set('superSecret', config.secret);
app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(morgan('dev'));

// mongoose
/*
mongoose.Promise = global.Promise;
mongoose.connect(config.database);
*/
var options = {
	dotfiles : 'ignore',
	extensions: ['htm','html'],
	index : false
};


app.use(express.static(path.join(__dirname, 'public'),options));

fs.readdirSync(routesFolder).forEach(file => {
  console.log(file);
  

  try
  {
		require(routesFolder + file)(app);
  }
  catch(e)
  {
  	console.log("route is empty");
  }

  
})

// active server

app.listen(port, function() {
  console.log("Express running in port: " + port);
});


app.get('*',function(req,res){
	res.sendfile("./public/index.html");
});