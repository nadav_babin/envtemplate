var gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  browserSync = require('browser-sync'),
  uglify = require('gulp-uglify'),
  rename = require('gulp-rename'),
  config = require('./config'),
  inject = require('gulp-inject');


gulp.task('index', function () {
  var target = gulp.src('./public/index.html');
  // It's not necessary to read the files (will speed up things), we're only after their paths:
  var sources = gulp.src(['!./public/bower_components/**/*.js','!./public/bower_components/**/*.css','./public/**/*.js', './public/**/*.css']);

  return target.pipe(inject(sources, {relative: true}))
    .pipe(gulp.dest('./public/'));
});


gulp.task('nodemon', function () {
  var stream = nodemon({ 
            script: 'server.js',
            ext: 'html js',
            ignore: ['ignored.js']})
 
  stream
      .on('restart', function () {
        console.log('restarted!');
            browserSync.reload({
            stream: false
          });
      })
      .on('crash', function() {
        console.error('Application has crashed!\n')
         stream.emit('restart', 1)  // restart the server in 10 seconds 
      })
});

gulp.task('browser-sync', ['nodemon'], function () {

  // for more browser-sync config options: http://www.browsersync.io/docs/options/
  browserSync({

    // informs browser-sync to proxy our expressjs app which would run at the following location
    proxy: 'http://localhost:' + config.port,

    // informs browser-sync to use the following port for the proxied app
    // notice that the default port is 3000, which would clash with our expressjs
    port: 4000
  });
});

gulp.task('js',['index'],function () {
  return gulp.src('public/**/*.js')
    // do stuff to JavaScript files
    //.pipe(uglify())
    //.pipe(gulp.dest('...'));
});

gulp.task('css',['index'],function () {
  return gulp.src('public/**/*.css')
    .pipe(browserSync.reload({ stream: true }));
})

gulp.task('bs-reload',['index'],function () {
  browserSync.reload();
});

gulp.task('develop', ['index','browser-sync'], function () {
  gulp.watch('public/**/*.js',   ['js', browserSync.reload]);
  gulp.watch('public/**/*.css',  ['css','index']);
  gulp.watch('public/**/*.html', ['bs-reload','index']);
});



